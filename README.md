# AlternativeKedamaMapData

Data for Alternative Unofficial Kedama Map; preview at https://3ec5k.csb.app/

## Notes

### 两 年 后

大概是把两年前写的从犄角旮旯里面翻出来了然后扔了v5的数据上去

访问 https://3ec5k.csb.app?world=v5

> （之前我到底都写了些什么💩 这💩居然还能跑2年）

### 鸣谢
@CitingNutra 提供v5 图源
@Kitakami_Oi 提供v4 voxelmap 缓存

### 已知问题

1. 由于没地方部署，附带的*CodeSandbox*运行环境很大，请使用PC端高版本Chrome或Firefox浏览器访问；
2. 由于使用 *GitLab*  作为图床，其在密集请求时会发生 `HTTP 429`，然后地图就黑了；此时不要着急，离开电脑，去喝一杯茶，然后再小范围缩放一下图；
3. 同样由于使用 *GitLab*  作为图床，部分地区可能无复访问，请使用***魔法***；
4. 地标颜色有问题是已知bug *但是两年前的💩山我确实无能为力*；

### 碎碎念

1. 关于更新：地图本身的更新需要人工跑图然后通过xaero's world map渲染(渲染出的大图有切图工具)；标记点(虽然现在没有)为`json`格式文件直接写在*CodeSandbox* project中，理论上都可以联系我修改；*实际上emm 毕业季体谅一下emm 不能保证反馈有多快*；
2. 关于隐私：两年前的项目，当时的环境确实没考虑去实现原始图覆盖功能；如果非常不愿意展现自己聚落或建筑可以联系我**告知坐标把所在128x128地图块剔除(效果是变成黑色)**
3. “你这个做的什么狗屎太丑了”—— emm确实我不是专业前端，能力有限；同时也欢迎贡献、修改或者另起炉灶；[图源](https://gitlab.com/DWCarrot/alternativekedamamapdata)和[代码](https://codesandbox.io/s/3ec5k)均是公开的；这种情况下如果同时愿意长期提供地图更新可以联系 @SilentDepth 鹅叔说这种情况他会接手(((
