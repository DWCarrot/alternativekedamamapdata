import os
import re
import argparse
import json

# need modify
callback = "config" # jsonp callback function name
root = "./src"  # root of json files to be convert to jsonp
tgt = "./jsonp" # output dir of jsonp
# end section 

if __name__ == "__main__":
    
    root = os.path.abspath(root)
    tgt = os.path.abspath(tgt)
    if os.path.exists(tgt):
        for dirpath, dirnames, filenames in os.walk(tgt):
            for filename in filenames:
                os.remove(os.path.join(dirpath, filename))
            for dirname in dirnames:
                os.remove(os.path.join(dirpath, dirname))

    for dirpath, dirnames, filenames in os.walk(root):
        for filename in filenames:
            fname, ext = os.path.splitext(filename)
            if ext == '.json':
                rel = os.path.relpath(dirpath, start=root)
                ifile = os.path.join(root, rel, fname + '.json')
                ofile = os.path.join(tgt, rel, fname + '.js')
                odir = os.path.join(tgt, rel)
                if not os.path.isdir(odir):
                    os.makedirs(odir)
                with open(ofile, 'w') as ofile:
                    with open(ifile, 'r', encoding='utf-8') as ifile:
                        data = json.load(ifile)
                        ofile.write(callback)
                        ofile.write('(')
                        json.dump(data, ofile)
                        ofile.write(')')
                print(rel, fname)

    