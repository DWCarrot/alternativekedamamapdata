import os
import re
import argparse
import json

def iter_dir(input_dir, url_root):
    p = re.compile(r'(-?\d+),(-?\d+).png')
    imgs = dict()
    input_dir, dirnames, _filenames = os.walk(input_dir).__next__()
    for dirname in dirnames:
        z = 0
        try:
            z = int(dirname)
        except Exception as e:
            continue
        for dirpath, _dirnames, filenames in os.walk(os.path.join(input_dir, dirname)):
            for filename in filenames:
                m = p.match(filename)
                if m is None:
                    continue
                x = int(m.group(1))
                y = int(m.group(2))
                filename = os.path.join(dirpath, filename)
                fileinfo = os.stat(filename)
                key = '{},{},{}'.format(z, x, y)
                url = '{}/{}/{},{}.png'.format(url_root, z, x, y)             
                size = int(fileinfo.st_size)
                mtime = int(fileinfo.st_mtime_ns / 1000000)
                imgs[key] = { 'url': url, 'size': size, 'mtime': mtime }
    return imgs


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputs')
    parser.add_argument('-r', '--root')
    parser.add_argument('-o', '--output')
    # parser.add_argument('-m', '--merge', nargs='+')
    # parser.add_argument('-O', '--merge_output')  
    args = parser.parse_args(args)
    imlist = iter_dir(os.path.abspath(args.inputs), args.root)
    with open(os.path.abspath(args.output), 'w') as ofile:
        json.dump(imlist, ofile)

    # merges = list()
    # for s in args.merge:
    #     sp = s.split(':')
    #     merges.append((sp[0], sp[1]))

    # json_data = dict()
    # for key, file in merges:
    #     with open(file) as ifile:
    #         d = json.load(ifile)
    #         json_data[key] = d

    # with open(args.merge_output, 'w') as ofile:
    #     ofile.write("config(")
    #     json.dump(json_data, ofile)
    #     ofile.write(")")

    

if __name__ == "__main__":
    import sys
    args = sys.argv[1:]
    if len(args) == 0:
        args = [
            '-o', './src/tiles.json',
            '-r', 'https://gitlab.com/DWCarrot/alternativekedamamapdata/-/raw/main/tiles',
            '-i', './tiles'
        ]
    main(args)
    
    

